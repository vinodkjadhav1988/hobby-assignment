<?php
class Hobby_model extends CI_Model{
    
    public function get_hobbies(){
        $this->db->where('user_id', $this->session->userdata("user_id"));
        
        $query = $this->db->get("hobbies");
        return $query->result();
    }

    public function get_sub_hobbies($hobby_id){
        $this->db->where('hobby_id', $hobby_id);
        
        $query = $this->db->get("sub_hobbies");
        return $query->result();
    }

    public function insert_hobby()
    {    

        #prepare data array to insert it into the user table
        $data = array(
            'hobby_name'    => $this->input->post('hobby'),
            'user_id'       => $this->session->userdata('user_id'),
            'status'        => 'active',
            'created_on'    => date("Y-m-d H:M:s")
        );

        $this->db->set('id', 'UUID()', FALSE);
        $insert_id = $this->db->insert('hobbies', $data);
        return  $insert_id;
    }

    public function insert_sub_hobby()
    {    
        #prepare data array to insert it into the user table
        $data = array(
            'sub_hobby_name' => $this->input->post('sub_hobby'),
            'hobby_id'       => $this->input->post('hobby_id'),
            'status'        => 'active',
            'created_on'    => date("Y-m-d H:M:s")
        );

        $this->db->set('id', 'UUID()', FALSE);
        $insert_id = $this->db->insert('sub_hobbies', $data);
        return  $insert_id;
    }
    
    public function delete_hobby($hobby_id){
        #delete hobbies
        $this->db->where('id', $hobby_id);
        $this->db->delete("hobbies");
    
        #delete sub hobbies too
        $this->db->where('hobby_id', $hobby_id);
        $this->db->delete("sub_hobbies");    
    }

    public function delete_sub_hobby($sub_hobby_id){
        #delete sub hobbies too
        $this->db->where('id', $sub_hobby_id);
        $this->db->delete("sub_hobbies");    
    }

    public function get_all_hobbies(){
        $this->db->select("CONCAT(u.first_name,' ',u.last_name) as name, GROUP_CONCAT(DISTINCT(h.hobby_name)) as hobbies, GROUP_CONCAT(sh.sub_hobby_name) as sub_hobbies ,count(DISTINCT(h.id)) as hobbies_count, COUNT(sh.id) as sub_hobbies_count")
             ->from('hobbies as h')
             ->join('sub_hobbies as sh', 'sh.hobby_id = h.id')
             ->join('users as u', 'u.id = h.user_id')
             ->group_by('h.user_id');

        $query = $this->db->get();
        return $query->result();
    }
}
?>