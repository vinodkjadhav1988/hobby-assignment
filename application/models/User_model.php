<?php
class User_model extends CI_Model{
    
    public function insert_user()
    {    
        #prepare data array to insert it into the user table
        $data = array(
            'first_name'        => $this->input->post('first_name'),
            'last_name'         => $this->input->post('last_name'),
            'email'             => $this->input->post('email'),
            'gender'            => $this->input->post('gender'),
            'password'          => md5($this->input->post('password')),
            'created_on'        => date("Y-m-d H:m:s")
        );

        $this->db->set('id', 'UUID()', FALSE);
        $this->db->insert('users', $data);
        $last_recored = $this->get_last_inserted_record();
        return $last_recored[0]->id;
    }    

    public function get_last_inserted_record(){
        $this->db->order_by("created_on", "desc");
        $this->db->limit(1);
        $query = $this->db->get("users");
        return $query->result();
    }

    public function validateUser(){
        $this->db->limit(1);
        $where_array = array(
                        'email'     => $this->input->post('email'),
                        'password'  => md5($this->input->post('password'))
                        );
        $this->db->where($where_array);
        $query = $this->db->get("users");
        return $query->result();
    }
}
?>