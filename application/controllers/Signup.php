<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	public function __construct(){
    	parent::__construct(); 
    }

	public function index()
	{
			
        if( $this->input->post() ){
        	$this->load->library('form_validation');

        	#set form validation rule
        	$this->form_validation->set_rules('first_name', 'First name', 'required');
        	$this->form_validation->set_rules('last_name', 'Last name', 'required');
        	$this->form_validation->set_rules('email', 'Email', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == FALSE){
				$this->load->view('sign_up');
            }else{

            	#insert users details
            	$this->load->model('user_model');
				$obj_user 	= new User_model();
	        	$user_id 	= $obj_user->insert_user();

	        	#set login user details in session
	        	$this->load->library('session');

	        	$set_session_data = array(
	        							'first_name' 	=> $this->input->post('first_name'),
	        							'user_id'		=> $user_id,	
	        							'login'			=> true	
	        						);

	        	$this->session->set_userdata($set_session_data);

	        	#redirect to hobbies page
	        	redirect("hobbies");
            }
        	
        }else{
			$this->load->view('sign_up');
        }

	}

	public function login(){

		if( $this->input->post() ){
        	$this->load->library('form_validation');

        	#set form validation rule
        	$this->form_validation->set_rules('email', 'Email', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == FALSE){
				$this->load->view('sign_in');
            }else{

            	#insert users details
            	$this->load->model('user_model');
				$obj_user 	= new User_model();
	        	$valid_user 	= $obj_user->validateUser();

	        	if( !empty($valid_user) ){

		        	#set login user details in session
		        	$this->load->library('session');

		        	$set_session_data = array(
		        							'first_name' 	=> $valid_user[0]->first_name,
		        							'user_id'		=> $valid_user[0]->id,	
		        							'login'			=> true	
		        						);

		        	$this->session->set_userdata($set_session_data);

		        	#redirect to hobbies page
		        	if( !empty($this->input->post('role') && $this->input->post('role') == "admin" ) ){
		        		redirect('admin/all_hobbies');
		        	}

		        	redirect("hobbies");
	        	}else{
	        		if( !empty($this->input->post('role') && $this->input->post('role') == "admin" ) ){
	        			echo "In correct Login Details";
						$this->load->view('admin/login');
	        		}else{
		        		echo "In correct Login Details";
						$this->load->view('sign_in');
	        			
	        		}
	        	}


            }
        	
        }else{
			$this->load->view('sign_in');
        }
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect("signup/login");
	}
}
