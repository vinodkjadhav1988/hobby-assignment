<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
    	parent::__construct(); 
		$this->load->model('hobby_model');
    }

	public function index()
	{
		$this->load->view('admin/login');
	}

	public function all_hobbies(){
		
		#get list of hobbies of all users
		$obj_hobby 		= new Hobby_model();
    	$rs_all_hobbies = $obj_hobby->get_all_hobbies();


    	$data['all_hobbies'] = $rs_all_hobbies;
    	
    	$this->load->view('admin/all_hobbies',$data);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect("admin");
	}
}
