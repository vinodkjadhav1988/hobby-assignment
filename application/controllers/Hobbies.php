<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hobbies extends CI_Controller {

	public function __construct()
    {
    	parent::__construct(); 
		$this->load->model('hobby_model');
    }


	public function index(){

		#if user is not login then redirect to sign in page
		if( !empty($this->session->userdata()) && $this->session->userdata('login') == true 	) {
			$data = array();
			$data['first_name'] = $this->session->userdata('first_name');
			
			#get list of hobbies
			$obj_hobby 	= new Hobby_model();
	    	$hobbies 	= $obj_hobby->get_hobbies();

	    	if( !empty($hobbies) ){
		    	$data['hobbies']	= $hobbies;

		    	#get subhobbies of hobbies
		    	#Loop over the each hobbies to the details
		    	foreach ($data['hobbies'] as $key => $hobby) {
		    		
		    		#query to the subhobbies
		    		$sub_hobbies 	= $obj_hobby->get_sub_hobbies($hobby->id);

		    		if( !empty($sub_hobbies) ){
		    			$data['hobbies'][$key]->sub_hobbies = $sub_hobbies;
		    		}
		    	}
	    	}

			$this->load->view('template/header', $data);
		 	$this->load->view('hobbies');
			
		}else{
			redirect('signup/login');
		}

	}

	public function save(){
		#if user is not login then redirect to sign in page
		if( !empty($this->session->userdata()) && $this->session->userdata('login') == true 	) {
			if( $this->input->post() ){
	        	$this->load->library('form_validation');

	        	#set form validation rule
	        	$this->form_validation->set_rules('hobby', 'Hobby', 'required');

	        	if ($this->form_validation->run() == FALSE){
		 			$data = array();
					$data['first_name'] = $this->session->userdata('first_name');
					$this->load->view('template/header', $data);
				 	$this->load->view('hobbies');
	            }else{

	            	#insert users details
					$obj_hobby 	= new Hobby_model();
		        	$user_id 	= $obj_hobby->insert_hobby();

		        	#redirect to hobbies page
		        	redirect("hobbies");
	            }
	        	
	        }else{
				$this->load->view('sign_up');
	        }
    	}else{
			redirect('signup/login');
		}
	}

	public function save_subhobby(){
		#if user is not login then redirect to sign in page
		if( !empty($this->session->userdata()) && $this->session->userdata('login') == true 	) {
			if( $this->input->post() ){
	        	$this->load->library('form_validation');

	        	#set form validation rule
	        	$this->form_validation->set_rules('sub_hobby', 'Sub Hobby', 'required');

	        	if ($this->form_validation->run() == FALSE){
		 			$data = array();
					$data['first_name'] = $this->session->userdata('sub_hobby');
					$this->load->view('template/header', $data);
				 	$this->load->view('hobbies');
	            }else{

	            	#insert subhobby details
					$obj_hobby 	= new Hobby_model();
		        	$user_id 	= $obj_hobby->insert_sub_hobby();

		        	#redirect to hobbies page
		        	redirect("hobbies");
	            }
	        }else{
				$this->load->view('sign_up');
	        }
    	}else{
			redirect('signup/login');
		}
	}

	public function delete($hobby_id){
		#if user is not login then redirect to sign in page
		if( !empty($this->session->userdata()) && $this->session->userdata('login') == true 	) {
			#delete hobby details
			$obj_hobby 	= new Hobby_model();
	    	$obj_hobby->delete_hobby($hobby_id);

	    	#redirect to hobbies page
	    	redirect("hobbies");
		}else{
			redirect('signup/login');
		}
	}

	public function delete_sub_hobby($sub_hobby_id){
		#if user is not login then redirect to sign in page
		if( !empty($this->session->userdata()) && $this->session->userdata('login') == true 	) {
			#delete subhobby details
			$obj_hobby 	= new Hobby_model();
	    	$obj_hobby->delete_sub_hobby($sub_hobby_id);

	    	#redirect to hobbies page
	    	redirect("hobbies");
		}else{
			redirect('signup/login');
		}
	}
}
