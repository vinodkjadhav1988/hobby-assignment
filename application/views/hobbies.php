<link href="<?php echo base_url()."application/assests/css/bootstrap.min.css"; ?>" rel="stylesheet" id="bootstrap-css">
<link href="<?php echo base_url()."application/assests/css/hobbies.css"; ?>" rel="stylesheet" id="bootstrap-css">
<script src="<?php echo base_url()."application/assests/js/bootstrap.min.js"; ?>"></script>
<script src="<?php echo base_url()."application/assests/js/jquery.js"; ?>"></script>
<script src="<?php echo base_url()."application/assests/js/validation.js";?>"></script>
<script src="<?php echo base_url()."application/assests/js/hobbies.js";?>"></script>

<div class="sidenav">
  <a href="#">Hobbies</a>
</div>

<div class="just-padding main">
  <div class="list-group list-group-root well">
    <?php echo form_open('hobbies/save', array('onsubmit' => 'return validateHobbies();', 'id' => 'hobby')); ?>
      <div class="add-hobbies">
        <label>Add Hobby:</label>
        <span class="text-danger"><?php echo form_error('hobby'); ?></span> 
        <input type="text" class="form-control " name="hobby" value="">
        
        <button type="submit" class="top-space" name="save_hobby">Save</button>
      </div>
    </form>

    <div>
      <hr>
    </div>

    <div class="list-hobbies">
      <span>List Hobbies:</span>
    </div>
    <?php
        if( !empty($hobbies) ){

          foreach ($hobbies as $key => $hobby) { ?>
            <span href="#" class="list-group-item"><?php echo ucwords($hobby->hobby_name); ?>
                <span class="action-hobby"> 
                  <a href="#" class="buttonDeco add-sub-hobby">Add Sub Hobby</a>
                  <span class="save-sub-hobby hide">
                    <?php echo form_open('hobbies/save_subhobby', array('onsubmit' => 'return validateSubHobbies();', 'id' => 'sub_hobby')); ?>
                    <input type="hidden" name="hobby_id" value="<?php echo $hobby->id;  ?>">
                    <input type="text" class="form-control" value="" name="sub_hobby" class=""/>
                    <button type="submit" class="save-sub-hobby top-space" name="save_sub_hobby">Save</button>
                    </form>
                  </span> |

                  <a href="<?php echo base_url("index.php/hobbies/delete/$hobby->id"); ?>" onClick="return deleteHooby();" class="buttonDeco">Delete</a>
                </span>
            </span>

            <!-- Loop over the sub hobbies to display -->
            <?php

              if( !empty($hobby->sub_hobbies) ){
                foreach ($hobby->sub_hobbies as $key => $sub_hobby) { ?>
                  <div class="list-group">
                      <div class="list-group">
                        <span class="list-group-item"><?php echo ucwords($sub_hobby->sub_hobby_name); ?>              
                          <a href="<?php echo base_url("index.php/hobbies/delete_sub_hobby/$sub_hobby->id"); ?>" onClick="return deleteSubHooby();" class="buttonDeco">Delete</a>
                        </span>
                      </div>
                  </div>
              <?php 
                }
              }
            ?>
      <?php
          }
      }else{
          echo "<label class='list-hobbies'>Hobbies Not added yet !</label>";
      }
    ?>    
  </div>
</div>