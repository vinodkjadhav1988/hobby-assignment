<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>

<span><h2>Welcome Admin  | <a href="<?php echo base_url("index.php/admin/logout"); ?>">Logout</a></h2>
<h4>All Hobbies:</h4>
<table style="width:100%">
  <tr>
    <th>Name</th>
    <th>Hobbies</th> 
    <th>No. of Hobbies</th> 
    <th>Sub Hobbies</th>
    <th>No. of SubHobbies</th>
  </tr>
  <?php
  	if( !empty($all_hobbies) ){
  		#Loop over the each hobbies to paint on screen
  		foreach ($all_hobbies as $key => $value) {
  			echo "<tr>
				    <td>$value->name</td>
				    <td>$value->hobbies</td>
				    <td>$value->sub_hobbies</td>
				    <td>$value->hobbies_count</td>
				    <td>$value->sub_hobbies_count</td>
				  </tr>";
  		}
  	}
  ?>
</table>

</body>
</html>
