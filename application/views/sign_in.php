<link href="<?php echo base_url()."application/assests/css/bootstrap.min.css"; ?>" rel="stylesheet" id="bootstrap-css">
<script src="<?php echo base_url()."application/assests/js/jquery.js"; ?>"></script>
<script src="<?php echo base_url()."application/assests/js/bootstrap.min.js"; ?>"></script>
<script src="<?php echo base_url()."application/assests/js/validation.js";?>"></script>

<p></p>
<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
	<h5 class="card-title mt-2">Sign in</h5>
</header>
<article class="card-body">
<?php echo form_open('signup/login', array('onsubmit' => 'return validateForm();', 'id' => 'sign-in')); ?>
	
	<div class="form-group">
		<label>Email</label>
		<input type="email" class="form-control" name="email" value="" placeholder="">
	</div> <!-- form-group end.// -->
	
	
	<div class="form-group">
		<label>Password</label>
	    <input class="form-control" name="password"  value="" type="password">
	</div> <!-- form-group end.// -->  
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
    </div> <!-- form-group// -->      
</form>
</article> <!-- card-body end .// -->
<div class="border-top card-body text-center">Don't have an account? <a href="<?php echo base_url("index.php/signup") ?>">Sign Up</a></div>
</div> <!-- card.// -->
</div> <!-- col.//-->

</div> <!-- row.//-->
</div> 
<!--container end.//-->
<br><br>
