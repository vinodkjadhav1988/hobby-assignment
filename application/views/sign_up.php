<link href="<?php echo base_url()."application/assests/css/bootstrap.min.css";?>" rel="stylesheet" id="bootstrap-css">
<script src="<?php echo base_url()."application/assests/js/bootstrap.min.js";?>"></script>
<script src="<?php echo base_url()."application/assests/js/jquery.js";?>"></script>
<script src="<?php echo base_url()."application/assests/js/validation.js";?>"></script>

<p></p>
<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
	<a href="<?php echo base_url("index.php/signup/login") ?>" class="float-right btn btn-outline-primary mt-1">Log in</a>
	<h5 class="card-title mt-2">Sign up</h5>
</header>
<article class="card-body">
<?php //echo validation_errors(); ?>
<?php echo form_open('signup', array('onsubmit' => 'return validateForm()', 'id' => 'sign-up')); ?>
	<div class="form-row">
		<div class="col form-group">
			<label>First name </label>  
			<span class="text-danger"><?php echo form_error('first_name'); ?></span> 
		  	<input type="text" class="form-control" placeholder="" name="first_name">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>Last name</label>
			<span class="text-danger"><?php echo form_error('last_name'); ?></span>
		  	<input type="text" class="form-control" placeholder="" name="last_name">
		</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
	<div class="form-group">
		<label>Email address</label>
		<span class="text-danger"><?php echo form_error('email'); ?></span>
		<input type="email" class="form-control" placeholder="" name="email">
	</div> <!-- form-group end.// -->
	<div class="form-group">
		<label class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="gender" value="male">
		  	<span class="form-check-label">Male</span>
		</label>
		<label class="form-check form-check-inline">
		 	<input class="form-check-input" type="radio" name="gender" value="female">
		 	<span class="form-check-label">Female</span>
		</label>
		<label class="form-check form-check-inline">
		 	<small>[optional]</small>
		</label>
	</div> <!-- form-group end.// -->
	
	<div class="form-group">
		<label>Create password</label>
		<span class="text-danger"><?php echo form_error('password'); ?></span>
	    <input class="form-control" type="password" name="password">
	</div> <!-- form-group end.// -->  
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Register  </button>
    </div> <!-- form-group// -->      
                                         
</form>
</article> <!-- card-body end .// -->
<div class="border-top card-body text-center">Have an account? <a href="">Log In</a></div>
</div> <!-- card.// -->
</div> <!-- col.//-->

</div> <!-- row.//-->


</div> 
<!--container end.//-->
<br><br>
