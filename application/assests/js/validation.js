function validateForm(){

	var valid = true;

	$("#sign-up, #sign-in").find("input[type=text]").each(function(index, field){
		if($(this).val() == ""){
			valid = false;
			$(this).addClass("is-invalid");
		}
	});
	
	var email 		=	$("input[name=email]").val();
	var password 	=	$("input[name=password]").val();

	if(validateEmail(email) === false){
		$("input[name=email]").addClass("is-invalid");
		valid = false;
	}

	if($("input[name=password]").val() == ""){
		valid = false;
		$("input[name=password]").addClass("is-invalid");
	}

	return valid
}

function validateHobbies(){
	
	if($("input[name=hobby]").val() == ""){
		$("input[name=hobby]").addClass("is-invalid");
		return false;
	}	
	return true;
}


function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


$(document).on("click",".save-sub-hobby",function() {
    console.log($(this).prev('input').val());
    if($(this).prev('input').val() ==""){
    	$(this).prev('input').addClass("is-invalid");
    	return false;
    }
    return true;
});